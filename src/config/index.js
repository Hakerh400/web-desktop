'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('assert');
const O = require('../omikron');

const {platform} = process;

const config = {
  'author': 'Hakerh400',
  
  'exe': {
    'electron': (
      platform === 'linux' ? '/usr/local/lib/node_modules/electron/dist/electron' :
      platform === 'win32' ? 'C:/Users/User/AppData/Roaming/npm/node_modules/electron/dist/electron.exe' :
      null
    ),
  },
};

module.exports = config;