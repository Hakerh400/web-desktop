'use strict';

const fs = require('fs');
const path = require('path');
const electron = require('electron');
const assert = require('../assert');
const O = require('../omikron');

const project = 'test';

const cwd = __dirname;
const globalProjectsDir = path.join(cwd, '../../..')

const webProjectsDir = path.join(globalProjectsDir, 'web/projects');
const projectDir = path.join(webProjectsDir, project);
const mainScriptPth = path.join(projectDir, 'main');

const lsDir = path.join(globalProjectsDir, 'local-storage');
const lsFile = path.join(lsDir, '001.json');

const init = () => {
  global.O = O;
  O.project = project;
  
  if(!fs.existsSync(lsDir))
    fs.mkdirSync(lsDir);
  
  let obj = null;
  
  const saveLs = () => {
    O.wfs(lsFile, JSON.stringify(obj));
  };
  
  const initLs = () => {
    obj = {};
    saveLs();
  };
  
  try{
    const str = O.rfs(lsFile, 1);
    obj = JSON.parse(str);
  }catch{
    initLs();
  }
  
  Object.defineProperty(global, 'localStorage', {
    value: new Proxy(obj, {
      set(t, k, v){
        obj[k] = v;
        saveLs();
        return 1;
      },
    }),
  });
};

init();

require(mainScriptPth);