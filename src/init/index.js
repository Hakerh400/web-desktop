'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const cp = require('child_process');
const O = require('../omikron');
const config = require('../config');

const cwd = __dirname;
const electronScript = path.join(cwd, 'electron.js');

const main = () => {
  const electronExe = config.exe.electron;
  
  // if(electronExe === null || !fs.existsSync(electronExe))
  //   throw new Error('Missing electron executable');
  
  const proc = cp.spawn(electronExe, [electronScript], {
    detached: true,
    stdio: 'ignore',
  });
  
  proc.unref();
};

main();